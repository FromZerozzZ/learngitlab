# 将CSV文件转换为字典
import pprint
import json
import pandas as pd

data = pd.read_csv("G:\\TestCSV\\data\\test.csv")
# data_dict = {col: list(data[col]) for col in data.columns}

# 与字典生成式功能一致
data_dict = {}
for col in data.columns:
    data_dict[col] = list(data[col])

print(json.dumps(data_dict))
# pprint.pprint(data_dict)
a = json.dumps(data_dict)

