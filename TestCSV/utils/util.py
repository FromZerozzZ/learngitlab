# import json
# from tencentcloud.common import credential
# from tencentcloud.common.profile.client_profile import ClientProfile
# from tencentcloud.common.profile.http_profile import HttpProfile
# from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
# from tencentcloud.ocr.v20181119 import ocr_client, models

# 引用鼠标键盘操作库
import pyautogui
from time import sleep
import csv

# import logging
# import logging.handlers
# import datetime

# logger = logging.getLogger('mylogger')
# logger.setLevel(logging.DEBUG)

# 记录日志
# 根据时间滚动

# def get_log():
#     rf_hander = logging.handlers.TimedRotatingFileHandler(
#         'log/all.log', when='midnight', interval=1, backupCount=7, atTime=datetime.time(0, 0, 0, 0))
#     rf_hander.setFormatter(logging.Formatter(
#         "%(asctime)s - %(levelname)s - %(message)s"))

#     f_handler = logging.FileHandler('log/error.log')
#     f_handler.setLevel(logging.ERROR)
#     f_handler.setFormatter(logging.Formatter(
#         "%(asctime)s-%(levelname)s-[:%(lineno)d] - %(message)s"))

#     logger.addHandler(rf_hander)
#     logger.addHandler(f_handler)
#     return logger


def pix1440_768To1920_1080(point):
    dx = 1.4
    dy = 1.41
    return (int(point[0]*dx), int(point[1]*dy))


# 读取CSV
def get_data(type, filename):
    if type == 'csv':
        print('读取csv')
        with open(filename, encoding='utf-8') as f:
            lists = csv.reader(f)
            my_data = []
            my_tuple = ()
            for row in lists:
                my_tuple = row
                my_data.append(my_tuple)
            return my_data
    else:
        print('type:类型不支持')

def runCSV(filename):
    print(filename)


def get_xdb(filePath, fileName):
    """
        filePath:xdb文件路径
        fileName：要打开的xdb文件名
    """
    print('path:', filePath)
    print('filename:', fileName)

    # 五次tab键,定位地址栏
    pyautogui.press('tab', presses=5)
    sleep(2)
    pyautogui.press('enter')

    # 写入路径
    pyautogui.write(filePath)
    sleep(2)
    pyautogui.press('enter')
    sleep(1)

    # 文件名
    pyautogui.keyDown('altleft')
    pyautogui.keyDown('n')
    pyautogui.keyUp('n')
    pyautogui.keyUp('altleft')
    pyautogui.write(fileName)
    sleep(2)
    pyautogui.press('enter')

# 腾讯云验证码
# def get_code(base64):
#     SecretId = 'AKIDNccMtLJaNOJ2Vm6pz2B7a9dheWNAmSyQ'
#     SecretKey = 'OWP6xmxubeSH62lGYa7PYVKxLYidJqGk'
#     try:
#         cred = credential.Credential(SecretId, SecretKey)
#         httpProfile = HttpProfile()
#         httpProfile.endpoint = "ocr.tencentcloudapi.com"

#         clientProfile = ClientProfile()
#         clientProfile.httpProfile = httpProfile
#         client = ocr_client.OcrClient(cred, "ap-guangzhou", clientProfile)

#         req = models.EnglishOCRRequest()
#         params = {
#             "ImageBase64": base64
#         }
#         req.from_json_string(json.dumps(params))

#         resp = client.EnglishOCR(req)
#         return resp.to_json_string()

#     except TencentCloudSDKException as err:
#         print('错误：', err)
