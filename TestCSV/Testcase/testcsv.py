# -*- coding : utf-8 -*-
from selenium import webdriver
from time import sleep
import unittest
import pyautogui as pg
import utils.util
from Testcase.page.v20LoginPage import V20LoginPage
from Testcase.page.v20hdfj import hdfjPage
from Testcase.page.v202DPage import V202DPage
from Testcase.page.basePage import BasePage

listcsv = utils.util.get_data('csv', 'G:\\TestCSV\\data\\test.csv')


class csvTest(unittest.TestCase):
    # 截图
    def save_img(self, imageName):
        self.driver.get_screenshot_as_file('{}/{}.png'.format("G:\\TestCSV\\img", imageName))

    # 选择场景
    def choose_screen(self):
        pg.moveTo(x=607, y=410, duration=0.25)
        pg.click(x=607, y=410, button='left')

    @classmethod
    def setUpClass(cls) -> None:
        # selenium 配置
        cls.options = webdriver.ChromeOptions()
        # 解决DevToolsActivePort文件不存在的报错
        cls.options.add_argument('--no-sandbox')
        # 谷歌文档提到需要加上这个属性来规避bug
        cls.options.add_argument('--disable-gpu')
        # 隐藏滚动条, 应对一些特殊页面
        cls.options.add_argument('--hide-scrollbars')

        # 临时写死
        cls.driver = webdriver.Chrome(options=cls.options)
        # 最大化
        cls.url = 'https://sj.yfway.com'
        cls.user = {'0': ['18273666762', '123456']}
        cls.driver.maximize_window()
        # 完成登录
        cls.loginPage = V20LoginPage(cls.driver)
        cls.loginPage.user_login(cls.url, cls.user['0'])
        cls.csvPage = BasePage(cls.driver)
        sleep(2)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.get_log(log_type='browser')
        cls.driver.quit()

    # 测试使用CSV数据绘制房间
    def test_csv(self):
        """使用CSV实现参数化"""
        # 登录
        login_page = V202DPage(self.loginPage)
        csv_page = hdfjPage(self.csvPage)
        # 选择方案
        csv_page.wait(20)
        # sleep(10)
        self.choose_screen()
        # 清除场景
        csv_page.wait(3)
        csv_page.clear_sences()
        csv_page.clear_canvas()
        csv_page.sure()
        # 点击【画单房间】
        csv_page.hdfj()
        csv_page.wait(1)
        # 点击
        pg.click(x=700, y=500)
        csv_page.wait(1)
        # 拖拽
        pg.moveTo(x=900, y=600)
        csv_page.wait(1)
        # 输入具体数值(画第一个房间)
        # pyautogui.write('6000')
        csv_page.write(listcsv[9][2])
        # pyautogui.press('enter')
        csv_page.press(listcsv[10][2])

        csv_page.wait(1)
        # pyautogui.write('4000')
        # pyautogui.press('enter')
        csv_page.write(listcsv[11][2])
        csv_page.press(listcsv[12][2])
        csv_page.wait(1)
        # 初始化视角
        csv_page.init()
        # sleep(20)
        # 画第二个房间
        csv_page.hdfj()
        pg.click(x=1545, y=232)
        pg.moveTo(x=1100, y=180)
        # pyautogui.write('4000')
        # pyautogui.press('enter')
        csv_page.write(listcsv[18][2])
        csv_page.press(listcsv[19][2])

        sleep(1)
        # pyautogui.write('3000')
        # pyautogui.press('enter')
        csv_page.write(listcsv[20][2])
        csv_page.press(listcsv[21][2])
        csv_page.wait(1)
        # 初始化视角
        csv_page.init()
        # 截图
        self.save_img('test_csv')
        print('绘制房间完成')
        sleep(3)
