# -*- coding: utf-8 -*-
import utils.util
from utils import util
from Testcase.page.basePage import BasePage
import pyautogui
import csv
from time import sleep
import sys
import time
import os

# 指定CSV文件并读取
listcsv = utils.util.get_data('csv', 'G:\\TestCSV\\data\\test.csv')


# 画单房间
class hdfjPage(BasePage):
    def __init__(self, login) -> None:
        BasePage.__init__(self, login.driver)
    # 按钮
    # 清除场景
    btn_clear_scenes = (listcsv[1][2], listcsv[1][3])
    # 清除画布
    btn_clear_canvas = (listcsv[2][2], listcsv[2][3])
    # 确认清除
    btn_sure = (listcsv[4][2], listcsv[4][3])
    # 画单房间
    btn_hdfj = (listcsv[6][2], listcsv[6][3])
    # 初始化视角
    btn_init = (listcsv[13][2], listcsv[13][3])
    # app > div > div.widget > div:nth-child(1) > div:nth-child(2)

    # 页面操作
    # 清除场景
    def clear_sences(self):
        self.click(self.btn_clear_scenes)
        self.wait(1)

    # 清除画布
    def clear_canvas(self):
        self.click(self.btn_clear_canvas)
        self.wait(1)

    # 确认
    def sure(self):
        self.click(self.btn_sure)
        self.wait(1)

    # 初始化视角
    def init(self):
        self.click(self.btn_init)
        self.wait(1)

    # 画单房间
    def hdfj(self):
        self.click(self.btn_hdfj)
        self.wait(1)
