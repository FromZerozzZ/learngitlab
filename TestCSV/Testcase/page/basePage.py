from selenium import webdriver
import time
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pyautogui


class BasePage(object):
    def __init__(self, driver) -> None:
        self.driver = driver
    # 生成日志
    def get_yflog(self):
        print("============= Auto WebUi test report START=============")
        for entry in self.driver.get_log('browser'):
            # print (entry)
            # if (entry['level'] == 'WARNING' or entry['Err'] == 'SEVERE'):
            # 只输出Error,忽略Warning.
            if entry['level'] == 'SEVERE':
                # numError += 1
                print(entry['source']+'\t')
                print(entry['message'])
                print('\n')
        print("============= Auto WebUi test report END=============")

    def get_selector(self, value):
        return self.driver.find_element_by_css_selector(value)
    
    def get_selectors(self, value, index):
        return self.driver.find_elements_by_css_selector(value)[index]

    def css_click(self, value, index=0):
        self.get_selectors(value, index).click()

    def open(self, url):
        self.driver.get(url)

    def excute_js(self, js):
        return self.driver.execute_script(js)

    def get_ems(self, *loc):
        return self.driver.find_elements(*loc)

    def get_em_text(self, *loc):
        return self.get_em(*loc).text
    
    def get_em(self, *loc):
        return self.driver.find_element(*loc)

    # 输入文本
    def input_text(self, text, *loc):
        self.get_em(*loc).clear()
        self.get_em(*loc).send_keys(text)

    # 等待
    def wait(self, time):
        self.driver.implicitly_wait(time)

    # 定位元素
    def find_element(self, locator, timeout=10):
        """
        定位单个元素,如果定位成功返回元素本身,如果失败,返回False
        :param timeout: 超时限制
        :param locator: 定位器,例如("id","id属性值")
        :return: 元素本身
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(EC.presence_of_element_located(locator))
            return element
        except:
            print(f"{locator}元素没找到")
            return False

    # 点击元素
    def click(self, locator):
        element = self.find_element(locator)
        element.click()

    # 输入元素
    def send_keys(self, locator, text):
        """
        元素输入
        :param locator: 定位器
        :param text: 输入内容
        :return:
        """
        element = self.find_element(locator)
        element.clear()
        element.send_keys(text)

    # 退出
    def close(self):
        time.sleep(2)
        self.driver.quit()

    # 鼠标操作
    # 鼠标点击
    def mouseclick(self, x, y):
        pyautogui.click(x=x, y=y)

    # 移动至
    def moveto(self, x, y):
        pyautogui.moveTo(x=x, y=y)

    # 输入
    def write(self, num):
        pyautogui.write(num)

    # 键盘操作
    def press(self, key):
        pyautogui.press(key)
