# -*- coding: utf-8 -*-
# import time
# from utils.util import get_code
# from PIL import Image  # easy_install pillow
# from io import BytesIO
# import base64
# import json
# import re
# sys.path.append(os.getcwd()+'\\test_case')
# print(sys.path)

from .basePage import BasePage
import sys
import os
from time import sleep


class V20LoginPage(BasePage):
    def __init__(self, driver) -> None:
        BasePage.__init__(self, driver)

    def user_login(self, url, user):
        """[summary]
        Args:
            url ([type]): [要打开的链接]
            user ([type]): [账号,密码]
        """
        username = user[0]
        password = user[1]
        text_user = ['css selector', 'input[name="mobilephone"]']
        text_password = ['css selector', 'input[name="password"]']
        btn_login = 'button'
        self.open(url)
        self.input_text(username, *text_user)
        sleep(1)
        self.input_text(password, *text_password)
        sleep(1)
        self.css_click(btn_login)
        print('登录完成')
