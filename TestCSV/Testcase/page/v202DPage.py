# -*- coding: utf-8 -*-
from utils import util
from .basePage import BasePage
import sys
import time
import os
from time import sleep
import pyautogui as pg
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

sys.path.append(os.getcwd()+'\\test_case')
winFile = './config/win'
pg.FAILSAFE = True  # 解决一些错误


class V202DPage(BasePage):
    def __init__(self, login) -> None:
        BasePage.__init__(self, login.driver)
        # self.driver=login.driver

    # loc
    # 上菜单栏
    # 文件
    btn_file = 'ul[role="menubar"]>li:nth-child(2)'
    btn_Client = ('xpath', '//ul/li[contains(text(),"客户信息")]')
    # 加载页
    loading_class = '.loading'

    # 左栏
    # 1、户型
    btn_huxing = ['.pageLink', 0]
    # 导入CAD文件
    btn_import_CAD = ['div.parts>div.part', 2]
    # 2、铺砖
    btn_puzhuan = ['.pageLink', 1]
    # 3、模型
    btn_moxing = ['.pageLink', 2]
    # 4、材质
    btn_caizi = ['.pageLink', 3]
    # 5、天花
    btn_yz_th = ['.pageLink', 4]

    # 清除场景
    btn_clean = ("css selector", "div.widget > div:nth-child(5) > div")
    btn_sure = ('xpath', '/html/body/div[3]/div/div[3]/button[2]/span')

    # 画单房间
    btn_hdfj = (
        'xpath', '//*[@id="app"]/div/div[9]/div[2]/div/div/div[2]/div/div[2]/div[4]')

    # 测试模型场景
    btn_test_model = (
        'xpath', '//*[@id="app"]/div/div[10]/div/div[2]/div/div[2]/div[1]/div[2]/div/span[1]')

    # 下栏
    # 初始化视角
    btn_initView = 'div[title="初始化视角"]'
    # 2d
    btn_2d = 'div[title="二维切换"]'
    # 3d
    btn_3d = 'div.widget-item:nth-child(3)'
    # 相机
    btn_camera = 'div[title="相机视角"]'
    # 相机第一视角
    btn_view1 = '.cameraView-content>div>div:nth-child(1)'

    # 铺贴设计
    # top
    # 方案
    # 换砖
    btn_ceramic_change_ceramic = 'div.top>p.switcher>i:nth-child(2)'
    # 水刀
    btn_ceramic_water_knife = 'div.top>p.switcher>i:nth-child(5)'
    # 退出
    btn_ceramic_exit = 'div.top>button:nth-child(3)'
    # 完成按钮
    btn_ceramic_complete = 'div.top>button:nth-child(4)'

    def test_upload_CAD(self, uploadTimes):
        print(f'循环测试上传CAD{uploadTimes}次')
        input_uploadFile = ['css selector',
                            'div.el-dialog__body>div.upload-demo>div>input']
        btn_upload = 'div[role="dialog"]>div.el-dialog__body>div.upload-demo>div>button'
        filePath = 'G:/bims-test-sj/scripts/data'
        fileName = 'Drawing1.dwg'
        print(f'上传文件路径为：{filePath}, 文件名为{fileName}')
        self.css_click(self.btn_import_CAD[0], self.btn_import_CAD[1])
        # self.input_text(filePath,*input_uploadFile)
        # 开始上传
        for i in range(uploadTimes):
            # self.wait_loading('click',('css selector',btn_upload))
            sleep(1)
            self.css_click(btn_upload)
            sleep(1)
            pg.press('tab', 5)
            pg.press('enter')
            pg.write(filePath)
            pg.press('enter')
            pg.keyDown('alt')
            pg.keyDown('n')
            pg.keyUp('n')
            pg.keyUp('alt')
            pg.write(fileName)
            pg.press('enter')

    def resetClient(self):
        self.css_click(self.btn_file)
        self.wait_loading("em_click", self.btn_Client)
        self.click(*self.btn_Client)
        self.css_click('.customer-title')

    def wait_loading(self, type='loading', locator=None):
        loading_class = ('class name', 'loading')
        # print('开始等待')
        start_time = time.time()
        wait = WebDriverWait(self.driver, 10)
        if type == 'loading':
            wait.until(EC.invisibility_of_element_located(
                loading_class), '等待超时')

        if type == 'em_click':
            if locator:
                wait.until(EC.element_to_be_clickable(locator), '等待超时')
            else:
                print('locator传入一个定位组')

        end_time = time.time()
        print(f'等待了{end_time-start_time}秒')
    # # 定制
    # def testCustomize(self, class_id):
    #     tmp_point = (1000, 850)
    #     tmp_point_menu = (1330, 880)
    #     if not os.path.isfile(winFile):
    #         tmp_point = (1000, 820)
    #         tmp_point_menu = (1330, 840)
    #     btn_dingzhi_class2 = [self.btn_dingzhi_class2, class_id]
    #     btn_dingzhi_class3 = ('css selector', 'div.main-cate-wrap.active>div>span.s1')
    #     btn_goback = 'div.el-card__header>div.clearfix>div'
    #     # 产品列表
    #     item_num = ('class name', 'product-list')
    #
    #     # 下一页
    #     btn_next = '.page-box>span:nth-child(3)'
    #     # 页码
    #     page_num = (
    #         'css selector', '.page-box>b>span')
    #
    #     product_num = 'div.product-list>div.el-image>img'
    #
    #     print(f'开始测试第{class_id}项定制柜')
    #
    #     self.css_click(self.btn_3d)
    #     sleep(2)
    #     self.css_click(self.btn_camera)
    #     sleep(1)
    #     self.css_click(self.btn_view1)
    #     sleep(1)
    #
    #     self.css_click(*self.btn_dingzhi)
    #     sleep(0.5)
    #     self.css_click(*btn_dingzhi_class2)
    #     sleep(0.5)
    #
    #     # 获得class3数量
    #     tmp_classes3 = len(self.get_ems(*btn_dingzhi_class3))
    #     class_start = 3
    #     for class3_id in range(class_start, tmp_classes3):
    #         # 吊柜
    #         if class_id == 0 and (class3_id == 1 or class3_id == 2):
    #             tmp_point = (1000, 550)
    #             tmp_point_menu = (1330, 580)
    #             if not os.path.isfile(winFile):
    #                 tmp_point = (1000, 580)
    #                 tmp_point_menu = (1330, 600)
    #         btn_class3 = ['div.main-cate-wrap.active>div>span.s1', class3_id]
    #         self.css_click(*btn_class3)
    #         sleep(1)
    #         pages = self.get_page(page_num)
    #         tmp_page = 1
    #         for page in range(tmp_page, pages):
    #             if tmp_page != 0:
    #                 self.click_pages(tmp_page)
    #                 tmp_page = 0
    #                 sleep(0.5)
    #             if page > 0:
    #                 self.css_click(btn_next)
    #             # 获得当前页的产品数
    #             item_num_tmp = len(self.get_ems(*item_num))
    #             if item_num_tmp == 0:
    #                 continue
    #             for item in range(item_num_tmp):
    #                 btn_product = [product_num, item]
    #                 self.css_click(*btn_product)
    #                 sleep(0.5)
    #                 pg.click(*tmp_point)
    #                 print(f'第{page}页，第{item}个产品')
    #                 sleep(1.5)
    #                 pg.click(*tmp_point)
    #                 sleep(0.8)
    #                 pg.press('del')
    #                 sleep(0.5)
    #
    #         self.css_click(btn_goback)
    #         sleep(0.5)

    def testCeramicCase(self):
        # 随便一点
        tmp_piont = (900, 600)
        tmp_piont_menu = (950, 565)
        # 第一个方案柜位置
        case1_piont = [400, 500]
        tmp_case1_piont = case1_piont.copy()
        case_dx = 160

        # 下一页
        btn_next = '.page-box>span:nth-child(3)'
        # 页码
        page_num = ('css selector', '.page-box>b>span')

        # 产品列表
        item_num = ('class name', 'list')

        # 方案下拉列表
        btn_if_sell = '.clearfix>.el-select:nth-child(1)'
        btn_where_do = '.clearfix>.el-select:nth-child(2)'
        btn_type_do = '.clearfix>.el-select:nth-child(3)'

        selectors1 = 2
        selectors2 = 4  # 暂时这个选项有bug出现了5项 第一项 0分类应该不存在
        selectors3 = [7, 7, 3, 2]  # 地面7种、墙面7种，木地板3种，淋浴石2种
        self.css_click(self.btn_3d)
        sleep(1)
        self.css_click(self.btn_camera)
        sleep(2)
        self.css_click(self.btn_view1)
        sleep(0.2)
        pg.click(*tmp_piont)
        sleep(1)
        pg.click(*tmp_piont_menu)
        sleep(3)
        if not os.path.isfile(winFile):
            pg.scroll(-10)
        else:
            pg.scroll(-50)
            pg.scroll(-50)

        # 防止div坑
        self.css_click(btn_if_sell)
        sleep(0.5)
        self.css_click(btn_where_do)
        sleep(0.5)
        self.css_click(btn_type_do)
        sleep(0.5)

        # 循环不可售、可售
        for select1 in range(0, selectors1):
            xpath_s1 = f'/html/body/div[5]/div[1]/div[1]/ul/li[{select1+1}]'

            btn_s1 = ['xpath', xpath_s1]
            # 首次开始不用切换
            if select1 > 0:
                print('切换select1')
                self.css_click(btn_if_sell)
                sleep(0.5)
                self.click(*btn_s1)
                sleep(0.5)
                self.css_click(btn_where_do)
                sleep(0.5)
                self.click(*btn_s2)
                sleep(0.5)
                self.css_click(btn_type_do)
                sleep(0.5)
                self.click(*btn_s3)
                sleep(0.5)

            # 循环地面、墙面，木地板，淋浴石
            for select2 in range(selectors2):
                xpath_s2 = f'/html/body/div[5]/div[1]/div[1]/ul/li[{select2+1}]'
                btn_s2 = ['xpath', xpath_s2]
                xpath_s3 = f'/html/body/div[5]/div[1]/div[1]/ul/li[{select3+1}]'
                btn_s3 = ['xpath', xpath_s3]
                # 首次开始不用切换
                if select2 > 0:
                    print('切换select2')
                    self.css_click(btn_if_sell)
                    sleep(0.5)
                    self.click(*btn_s1)
                    sleep(0.5)
                    self.css_click(btn_where_do)
                    sleep(0.5)
                    self.click(*btn_s2)
                    sleep(0.5)
                    self.css_click(btn_type_do)
                    sleep(0.5)
                    self.click(*btn_s3)
                    sleep(0.5)
                # 拿出地面、墙面，木地板，淋浴石里的select3行数7, 7, 3, 2
                selects3 = selectors3[select2]

                # 循环select3  /html/body/div[5]/div[1]/div[1]/ul/li[2]
                for select3 in range(selects3):
                    # 坑死我的定位
                    xpath_s3 = f'/html/body/div[5]/div[1]/div[1]/ul/li[{select3+1}]'
                    btn_s3 = ['xpath', xpath_s3]
                    # 首次开始不用切换
                    if select3 > 0:
                        self.css_click(btn_if_sell)
                        sleep(0.5)
                        self.click(*btn_s1)
                        sleep(0.5)
                        self.css_click(btn_where_do)
                        sleep(0.5)
                        self.click(*btn_s2)
                        sleep(0.5)
                        self.css_click(btn_type_do)
                        sleep(0.5)
                        self.click(*btn_s3)
                        sleep(0.5)
                    # 获得页数
                    pages = self.get_page(page_num)
                    # pages = 3

                    # 循环页数
                    for page in range(pages):
                        if page > 0:
                            self.css_click(btn_if_sell)
                            sleep(0.5)
                            self.click(*btn_s1)
                            sleep(0.5)
                            self.css_click(btn_where_do)
                            sleep(0.5)
                            self.click(*btn_s2)
                            sleep(0.5)
                            self.css_click(btn_type_do)
                            sleep(0.5)
                            self.click(*btn_s3)
                            sleep(0.5)
                        # 首页不用切换
                        if page > 0:
                            self.click_pages(page)

                        sleep(1)
                        # 获得当前页的产品数
                        item_num_tmp = len(self.get_ems(*item_num))
                        print(f'当前页有{item_num_tmp}个产品')
                        # item_num_tmp = 8
                        # 无产品跳过
                        if item_num_tmp == 0:
                            continue

                        # 循环产品列表
                        for item in range(item_num_tmp):
                            print(select1, select2, select3, page, item)
                            js = f'document.getElementsByClassName("list")[{item}].childNodes[2].className="focused"'
                            self.excute_js(js)
                            sleep(0.2)
                            self.css_click('.focused')
                            pg.click(*tmp_case1_piont)
                            tmp_case1_piont[0] = tmp_case1_piont[0]+case_dx
                            sleep(2)

                        tmp_case1_piont = case1_piont.copy()
                        self.css_click(self.btn_ceramic_exit)
                        sleep(1.5)
                        self.css_click(self.btn_camera)
                        sleep(0.5)
                        self.css_click(self.btn_view1)
                        sleep(0.2)
                        # self.render()
                        pg.click(*tmp_piont)
                        sleep(1)
                        pg.click(*tmp_piont_menu)
                        sleep(3)
                        if not os.path.isfile(winFile):
                            pg.scroll(-10)
                        else:
                            pg.scroll(-50)
                            pg.scroll(-50)
                        # 防止div坑
                        self.css_click(btn_if_sell)
                        sleep(0.5)
                        self.css_click(btn_where_do)
                        sleep(0.5)
                        self.css_click(btn_type_do)
                        sleep(0.5)
                select3 = 0
            select2 = 0
        # list_cases = self.checkCeramicCase()
        # print(list_cases)

    def testCeramicChange(self, tab_sell=0):
        # 随便一点
        tmp_piont = (900, 600)
        tmp_piont_menu = (950, 565)
        tmp_point_center = (1920/2-100, 1080/2)
        # 第一个方案柜位置
        case1_piont = [400, 500]
        tmp_case1_piont = case1_piont.copy()
        case_dx = 150
        # 页码
        page_num = (
            'css selector', '.page-box>b>span')

        # 新规格
        btn_new_ceramic = 'div.el-message-box__btns>button:last-child'

        # 筛选
        btn_ceramic_sell = 'button.el-button.el-button--default.el-button--mini'
        btn_can_sell = ['p>div.el-row>div:nth-child(2)', 0]
        btn_canot_sell = ['p>div.el-row>div:nth-child(1)', 0]

        # 产品列表
        item_num = ('class name', 'list')

        self.css_click(self.btn_3d)
        sleep(1)
        self.css_click(self.btn_camera)
        sleep(2)
        self.css_click(self.btn_view1)
        sleep(1)
        pg.click(*tmp_piont)
        sleep(1)
        pg.click(*tmp_piont_menu)
        self.wait_loading()
        pg.moveTo(*tmp_point_center)
        if not os.path.isfile(winFile):
            pg.scroll(-8)
        else:
            pg.scroll(-50)
            pg.scroll(-50)
        self.css_click(self.btn_ceramic_change_ceramic)
        sleep(1)

        if tab_sell == 1:
            self.css_click(btn_ceramic_sell)
            sleep(0.5)
            self.css_click(*btn_can_sell)
            sleep(0.5)
            pg.click(*tmp_piont)

        if tab_sell == 0:
            self.css_click(btn_ceramic_sell)
            sleep(0.5)
            self.css_click(*btn_canot_sell)
            sleep(0.5)
            pg.click(*tmp_piont)

        pages = self.get_page(page_num)
        print(f'共有{pages}页')

        for page in range(0, pages):
            if page > 0:
                if tab_sell == 1:
                    self.css_click(btn_ceramic_sell)
                    sleep(0.2)
                    self.css_click(*btn_can_sell)
                    sleep(0.5)
                    pg.click(*tmp_piont)
                    self.click_pages(page)
                if tab_sell == 0:
                    self.css_click(btn_ceramic_sell)
                    sleep(0.2)
                    self.css_click(*btn_canot_sell)
                    sleep(0.5)
                    pg.click(*tmp_piont)
                    self.click_pages(page)

            item_num_tmp = len(self.get_ems(*item_num))
            print(f'当前页有{item_num_tmp}个产品')

            for item in range(0, item_num_tmp):
                print(f'正在测试第{page+1}页，第{item+1}个砖')
                if page == 99 and item == 0:
                    pass
                else:
                    js = f'document.getElementsByClassName("list")[{item}].childNodes[2].className="focused"'
                    self.excute_js(js)
                    sleep(0.2)
                    self.css_click('.focused')
                    pg.click(*tmp_case1_piont)
                    sleep(0.2)
                    # document.getElementsByClassName("el-message-box__wrapper")[0].style[1]
                    # document.getElementsByClassName("el-message-box__wrapper").length
                    js2 = f'return document.getElementsByClassName("el-message-box__wrapper").length'
                    js3 = f'return document.getElementsByClassName("el-message-box__wrapper")[0].style[1]'
                    test_div2 = self.excute_js(js2)
                    test_div3 = None
                    if test_div2 != 0:
                        test_div3 = self.excute_js(js3)
                    print(test_div2, test_div3)
                    if test_div2 == 1 and test_div3 == None:
                        self.css_click(btn_new_ceramic)

                tmp_case1_piont[0] = tmp_case1_piont[0]+case_dx
                sleep(2)
            tmp_case1_piont = case1_piont.copy()
            self.css_click(self.btn_ceramic_complete)
            self.css_click(self.btn_camera)
            self.css_click(self.btn_view1)
            self.render()
            sleep(0.2)
            pg.click(*tmp_piont)
            sleep(1)
            pg.click(*tmp_piont_menu)
            sleep(3)
            pg.moveTo(*tmp_point_center)
            if not os.path.isfile(winFile):
                pg.scroll(-8)
            else:
                pg.scroll(-50)
                pg.scroll(-50)
            self.css_click(self.btn_ceramic_change_ceramic)
            sleep(0.5)

    def testCeramicWarterKnife(self):

        page_num = (
            'css selector', '.page-box>b>span')
        # 产品列表
        item_num = ('class name', 'list')
        input_water_l = (
            'css selector', 'div[aria-label="水刀拼花尺寸"]>div>div>div>input[placeholder="请输入长度"]')
        input_water_w = (
            'css selector', 'div[aria-label="水刀拼花尺寸"]>div>div>div>input[placeholder="请输入宽度"]')
        btn_water_size_ok = 'div[aria-label="水刀拼花尺寸"]>div.el-dialog__footer>div.dialog-footer>button:last-child'
        tmp_point_C = (800, 837)  # linux
        # tmp_point_C=(800,800)# win
        tmp_point_center = (1920/2, 1080/2)
        tmp_point_menu = [tmp_point_C[0]+50, tmp_point_C[1]-45]
        tmp_point_A = [tmp_point_center[0]/2+50, tmp_point_center[1]+50]
        tmp_point_B = [tmp_point_center[0]/2*3-50, tmp_point_center[1]+50]

        # 开始
        self.css_click(self.btn_initView)
        pg.click(*tmp_point_C)
        sleep(1)
        pg.click(*tmp_point_menu)
        sleep(2)

        self.css_click(self.btn_ceramic_water_knife)
        sleep(1)

        pages = self.get_page(page_num)
        # 临时测试
        # pages = 2
        print(f'共有产品{pages}页')

        for page in range(5, pages):
            # 获得当前页的产品数
            if page > 0:
                self.click_pages(page)
            item_num_tmp = len(self.get_ems(*item_num))
            print(f'当页面有{item_num_tmp}项产品')

            for item in range(0, item_num_tmp):
                print(f'正在处理第{page+1}页，第{item+1}个产品')
                if item != 0:
                    if page > 0 and item % 2 == 0:
                        self.click_pages(page)

                if item % 2 == 0:
                    pg.click(*tmp_point_A)
                    pg.press('del')
                    sleep(1)
                    pg.click(*tmp_point_B)
                    pg.press('del')
                    sleep(1)

                js = f'document.getElementsByClassName("list")[{item}].childNodes[2].className="focused"'
                self.excute_js(js)
                sleep(0.2)
                self.css_click('.focused')
                sleep(0.5)
                self.input_text('2000', *input_water_l)
                self.input_text('2000', *input_water_w)
                self.css_click(btn_water_size_ok)
                sleep(2)

                if (item+1) % 2 != 0:
                    pg.click(*tmp_point_A)
                    sleep(0.5)
                else:
                    pg.click(*tmp_point_B)
                    sleep(0.5)

                    self.css_click(self.btn_ceramic_complete)
                    sleep(1)
                    self.css_click(self.btn_initView)
                    pg.click(*tmp_point_C)
                    sleep(1)
                    pg.click(*tmp_point_menu)
                    sleep(2)
                    self.css_click(self.btn_ceramic_water_knife)
                    sleep(1)

    # 选择客户与场景

    def choose_screen(self, screen):
        locScreen1 = '.mask[style="display: block;"]'
        print(f'选择了第{screen}场景')

        # 显示打开按钮
        openfile = 'document.getElementsByClassName("mask")['+str(
            screen-1)+'].style.display="block"'
        self.excute_js(openfile)
        self.wait_loading('em_click', ('css selector', locScreen1))
        self.css_click(locScreen1)
        self.wait_loading()

    # 获取产品页
    def get_page(self, loc):
        pages = self.get_em_text(*loc)
        pages = pages.replace(" ", "")
        pages = int(pages.split("/")[1])
        return pages

    # 测试门类
    # 替换门
    def choose_if_sell(self, if_sell):
        btn_if_sell = 'input[placeholder="可售情况"]'
        temp_point = 'div.product-box>div>div>div>span'
        if if_sell == 0:
            print('可售')
            # btn_can_sel = (
            #     'xpath', '//div/div/div/ul/li/span[contains(text(),"可售")]')
            #     #//div/div/div/ul/li/span[contains(text(),"可售")]
            #     #//div/div/div/ul/li/span[contains(text(),"不可售")]

        if if_sell == 1:
            print('不可售')
            btn_can_sel = (
                'xpath', '//div/div/div/ul/li/span[contains(text(),"不可售")]')

            self.css_click(btn_if_sell)
            self.wait_loading('click', btn_can_sel)
            self.click(*btn_can_sel)
            self.css_click(temp_point)

    def change_door(self, select, page, item):
        print('click', select, page, item)

        self.choose_if_sell(select)
        sleep(0.5)
        self.click_pages(page)
        sleep(1)
        js = f'document.getElementsByClassName("product-list")[{item}].childNodes[3].className="focused"'
        self.excute_js(js)
        self.wait_loading('click', ('css selector', '.focused'))
        self.css_click('.focused')
        sleep(0.5)

    def click_pages(self, page):
        # 下一页
        btn_next = 'div.page-box>span:last-child'
        input_text = ['css selector', 'div.page-box>b>input']
        if page > 0:
            print(f'跳到第{page+1}页')
            self.input_text(page+1, *input_text)
            pg.press('enter')
            # for click in range(1, page+1):
            #     print("click", click)
            #     self.css_click(btn_next)

    # 测试门
    def check_item(self, selects):
        print(f'check_item:有{selects}可售选择')
        items = []
        items_tmp = []
        btn_if_sell = 'input[placeholder="可售情况"]'
        btn_can_sel = (
            'xpath', '/html/body/div[3]/div[1]/div[1]/ul/li[2]/span')
        page_num = (
            'css selector', '.page-box>b>span')

        # 产品列表
        item_num = ('class name', 'product-list')

        for select in range(selects):
            if select == 1:
                self.css_click(btn_if_sell)
                sleep(1.2)
                self.click(*btn_can_sel)
                sleep(1)
                print('不可售')
            if select == 0:
                print('可售')
            pages = self.get_page(page_num)
            sleep(0.5)
            print(f'当前可售模型下有{pages}页')
            for page in range(pages):
                btn_next = '.page-box>span:nth-child(3)'
                if page > 0:
                    self.css_click(btn_next)
                    sleep(0.8)
                # 获得当前页的产品数
                item_num_tmp = len(self.get_ems(*item_num))
                # print(f'当前第{page},有{item_num_tmp}个产品')
                items_tmp.append(item_num_tmp)
            items.append(items_tmp)
            items_tmp = []
            pages = 0

        return items

    def test_door(self, model_door):
        if model_door == 6:
            # 初始化阳台门位置
            door_dx = 180
            door_dy = 40
            dx = 404
            selects = 1
            doors_inline = 2
            point_menu = [810, 590]

            if not os.path.isfile(winFile):
                point_menu = [810, 605]

        if model_door == 3 or model_door == 4 or model_door == 5:
            # 初始化推拉门 折叠门位置
            door_dx = 180
            door_dy = 40
            dx = 229
            selects = 2
            doors_inline = 4
            point_menu = [720, 590]

            if not os.path.isfile(winFile):
                point_menu = [720, 605]

        if model_door == 1:
            # 初始化单开门位置
            door_dx = 180
            door_dy = 40
            dx = 111
            doors_inline = 8
            selects = 1
            point_menu = [680, 590]

            if not os.path.isfile(winFile):
                point_menu = [680, 605]

        if model_door == 2:
            # 初始化双开门位置
            door_dx = 180
            door_dy = 40
            dx = 148
            selects = 1
            doors_inline = 8
            point_menu = [700, 590]
            if not os.path.isfile(winFile):
                point_menu = [700, 605]

        point_door = [point_menu[0]-door_dx, point_menu[1]-door_dy]

        self.css_click(self.btn_2d)
        self.css_click(self.btn_initView)

        pg.click(*point_door)
        sleep(0.5)
        pg.click(*point_menu)
        sleep(0.5)

        # list_items = self.check_item(selects)
        # print(f'有{len(list_items)}个销售项')
        # print(f'可售有{len(list_items[0])}页，可售产品列表{list_items[0]}')
        # print(f'不可售有{len(list_items[1])}页，不可售产品列表{list_items[1]}')

        # 临时测试
        list_items = [[8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                       8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 5]]
        selects = len(list_items)
        for select in range(0, selects):
            tmp_items = list_items[select]
            pages = len(tmp_items)
            for page in range(12, pages):
                items = tmp_items[page]
                if items == 0:
                    continue
                point_door_tmp = []
                point_menu_tmp = []
                point_door_tmp = point_door.copy()
                point_menu_tmp = point_menu.copy()
                for item in range(items):

                    pg.click(*point_door_tmp)
                    sleep(0.5)
                    pg.click(*point_menu_tmp)
                    sleep(1)
                    self.change_door(select, page, item)
                    point_door_tmp[0] = point_door_tmp[0]+dx
                    point_menu_tmp[0] = point_menu_tmp[0]+dx
                    # 3 4 5 6类门，一行能替换的数量不满8个，中途去渲染，然后重坐标继续替换。
                    if item > 0 and (item+1) % doors_inline == 0:
                        print('去渲染')
                        # 随便一点，关闭门的窗口
                        pg.click(1024, 225)
                        sleep(0.2)
                        # 重置坐标
                        point_door_tmp = point_door.copy()
                        point_menu_tmp = point_menu.copy()
                        self.css_click(self.btn_3d)
                        sleep(1)
                        self.css_click(self.btn_camera)
                        sleep(1)
                        self.css_click(self.btn_view1)
                        sleep(2)
                        self.render()
                        self.css_click(self.btn_2d)
                        self.css_click(self.btn_initView)

        # 移动文件上弹出菜单
        em = self.get_selector(self.btn_file)
        ActionChains(self.driver).move_to_element(em).perform()
        sleep(0.5)
        self.click(*self.btn_Client)
        sleep(1)

    # 导出全部MZ
    def openScreenID(self, screenID):
        btn_openScreenID = 'li[role="menuitem"]:nth-child(11)'
        self.css_click(btn_openScreenID)
        screenID = screenID[0]
        pg.write(screenID)
        pg.press('enter')

    def exportAllMZ(self, screenID):
        print('导出场景')
        print(screenID)
        btn_exportAllRoom = 'li[role="menuitem"]:nth-child(10)'
        # 移动文件上弹出菜单
        em = self.get_selector(self.btn_file)
        ActionChains(self.driver).move_to_element(em).perform()
        # 打开指定id场景
        self.openScreenID(screenID)
        # 时间足够长，特别大场景
        sleep(20)
        ActionChains(self.driver).move_to_element(em).perform()
        # 导出全部空间
        self.css_click(btn_exportAllRoom)
        sleep(10)

    # 清空场景
    def cleanScreen(self):
        self.click(*self.btn_clean)
        self.click(*self.btn_sure)
        sleep(1)
        print('清空场景完成')

    # 渲染
    def render(self):
        working_time = int(time.strftime('%H', time.localtime()))
        debug = True
        if (0 < working_time < 9) or debug == True:
            # 渲染按钮
            btn_render = (
                'xpath', '//*[@id="app"]/div/aside/section/ul/li[6]/a')
            # 光速
            btn_gs_tab = (
                'xpath', '//*[@id="app"]/div/div[6]/div[2]/div[1]/div[1]')
            # 光子tab
            btn_gz_tab = (
                'xpath', '//*[@id="app"]/div/div[6]/div[2]/div[1]/div[2]')
            # 3d
            btn_3d = ('xpath', '//*[@id="app"]/div/div[5]/div[1]/div[3]')
            # 提交渲染
            btn_render_ok = ('xpath', '//*[@id="app"]/div/div[6]/div[5]')
            # 退出
            btn_exit_render = (
                'xpath', '//*[@id="app"]/div/div[6]/div[1]/button')
            # sleep(5)
            self.click(*btn_render)
            self.wait_loading()
            self.click(*btn_gs_tab)
            self.wait_loading()
            self.click(*btn_render_ok)
            sleep(1)
            self.click(*btn_gz_tab)
            self.wait_loading()
            self.click(*btn_render_ok)
            sleep(1)
            self.click(*btn_exit_render)
            # sleep(1)
            print("渲染完成")
        else:
            print('工作时间,跳过渲染')

    def enter_yzth(self, testType):
        """[summary]
        Args:
            testType ([type]): [thmx(天花模型)|zzth(整装天花)|lkb(铝扣板)]
        """
        btn_yz_th_thmx = ['.switcher>i', 0]
        btn_yz_th_zzth = ['.switcher>i', 2]
        btn_yz_th_lkb = ['.switcher>i', 3]

        # 开始
        # self.wait_loading('click', ('css selector', self.btn_yingZhuang[0]))
        # self.css_click(self.btn_yingZhuang[0], self.btn_yingZhuang[1])
        # 点击左栏【天花】
        self.wait_loading('click', ('css selector', self.btn_yz_th[0]))
        self.css_click(self.btn_yz_th[0], self.btn_yz_th[1])
        self.wait_loading()
        sleep(1)
        if testType == 'zzth':
            moduleType = btn_yz_th_zzth
        if testType == 'lkb':
            moduleType = btn_yz_th_lkb
        if testType == 'thmx':
            moduleType = btn_yz_th_thmx
        # self.click(*moduleType)
        self.css_click(moduleType[0], moduleType[1])
        self.css_click(self.btn_initView)
        print('进入：', testType)

    def test_yzth(self, testType):
        # 我本机是1920*1080
        RoomA = (820, 500)  # 本机
        RoomB = (870, 500)  # 本机
        pointA = (890, 320)  # 本机
        pointB = (1500, 920)  # 本机
        pointC = (980, 640)  # 本机
        if not os.path.isfile(winFile):
            RoomA = (820, 500)  # linux
            RoomB = (870, 500)  # linux
            pointA = (880, 320)  # linux
            pointB = (1500, 920)  # linux
            pointC = (980, 640)  # linux

        # 单房间
        btn_singleRoom = 'p>label:last-child'
        # 下一页
        page_num = ('css selector', '.page-box>b>span')
        item_num = ('class name', 'list-item')
        model_zzth_one = '.list-item.focused'
        item_lacator = ('css selector', model_zzth_one)
        # btn_complete = (
        #     'xpath', '//*[@id="app"]/div/div[6]/div[1]/div[1]/button')
        btn_complete = '.top>button'

        # 开始
        self.enter_yzth(testType)

        # 统计当前list-item数量，页数
        pages = None
        items = None
        pages = self.get_page(page_num)

        for page in range(0, pages):
            if page + 1 > 1:
                self.click_pages(page)
            # 获得当前页面list-item数量
            items = len(self.get_ems(*item_num))

            for item in range(items):
                js = f'document.getElementsByClassName("list-item")[{item}].className="list-item focused"'
                if testType == 'zzth' or testType == 'lkb':
                    if (item + 1) % 2 != 0:
                        self.css_click(btn_singleRoom)
                        # 先清空上次的天花
                        sleep(1)
                        self.css_click(self.btn_2d)
                        self.css_click(self.btn_initView)
                        pg.click(*RoomA)
                        pg.press('del')
                        sleep(0.5)
                        pg.click(*RoomB)
                        pg.press('del')
                        sleep(0.5)
                        # 这个点是为了防止某些模型前两个点无法点中
                        pg.click(*pointC)
                        pg.press('del')
                        sleep(0.5)
                        self.chooseItems(page, item, js, item_lacator)
                        pg.click(*RoomA)
                        self.css_click(self.btn_2d)
                        self.css_click(self.btn_initView)
                    else:
                        self.chooseItems(page, item, js, item_lacator)
                        pg.click(*RoomB)
                        self.css_click(btn_complete)
                        self.wait_loading()
                        self.css_click(self.btn_3d)
                        sleep(2)
                        self.css_click(self.btn_camera)
                        sleep(0.5)
                        self.css_click(self.btn_view1)
                        sleep(1)
                        # self.render()
                        sleep(1)
                        self.enter_yzth(testType)

                if testType == 'thmx':
                    self.css_click(self.btn_2d)
                    self.css_click(self.btn_initView)
                    sleep(1)
                    pg.click(*RoomA)
                    pg.press('del')
                    sleep(0.5)
                    pg.click(*RoomB)
                    pg.press('del')
                    sleep(0.5)
                    # 这个点是为了防止某些模型前两个点无法点中
                    pg.click(*pointC)
                    pg.press('del')
                    sleep(0.5)
                    self.chooseItems(page, item, js, item_lacator)
                    sleep(1)
                    pg.click(*pointA)
                    sleep(1)
                    pg.click(*pointB)
                    sleep(1)
                    self.css_click(btn_complete)
                    self.wait_loading()
                    self.css_click(self.btn_3d)
                    sleep(2)
                    self.css_click(self.btn_camera)
                    sleep(0.5)
                    self.css_click(self.btn_view1)
                    sleep(1)
                    # self.render()
                    sleep(1)
                    self.enter_yzth(testType)

        self.css_click(btn_complete)

    def chooseItems(self, page, item, js, locator):
        print(f'点击第{page+1}页，第{item+1}项')
        self.click_pages(page)
        self.excute_js(js)
        self.wait_loading('click', locator)
        self.css_click(locator[1])

    def openFile(self, filePath, fileName):
        """
        filePath:xdb文件路径

        fileName：要打开的xdb文件名
        """
        self.click(*self.btn_openFile)
        sleep(1)
        util.get_xdb(filePath, fileName)
        sleep(5)
