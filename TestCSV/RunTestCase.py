import time
import unittest
import os
from selenium import webdriver
from BeautifulReport import BeautifulReport
import sys


# 指定测试用例路径
casePath = 'G:\\TestCSV\\Testcase'

# 使用正则匹配用例
discover = unittest.defaultTestLoader.discover(start_dir=casePath,
                                               pattern='test*.py',
                                               top_level_dir=None,)

if __name__ == '__main__':
    # 指定测试报告目录
    rpdir = "TestReport"
    # 测试报告子目录
    rpdir2 = os.path.join(rpdir, time.strftime('%Y-%m-%d', time.localtime()))
    if not os.path.exists(rpdir2):
        os.mkdir(rpdir2)
    # 测试报告名
    rpdst = os.path.join(rpdir2, '{}.html'.format(time.strftime('%H-%M-%S', time.localtime())))
    file_obj = open(rpdst, 'w+', encoding='utf-8')

    # 创建测试报告对象
    result = BeautifulReport(discover)
    # 定义测试报告路径及相关描述
    result.report(filename=rpdst,
                  description='【美家大师】测试',
                  theme='theme_default',)
